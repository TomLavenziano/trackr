# Trackr

<img src="./res/TrackrLogo_Circle.png" align="left" width="292px" height="292px" hspace="50"/>

 #### A simple event tracker and planner.



[![pipeline status](https://gitlab.com/TomLavenziano/trackr/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/TomLavenziano/trackr/commits/master) [![coverage report](https://gitlab.com/TomLavenziano/trackr/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/TomLavenziano/trackr/commits/master) [![license: MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tom.mit-license.org/) [![Under Development](https://img.shields.io/badge/under-development-orange.svg?style=flat-square)](https://gitlab.com/TomLavenziano/trackr)

**Trackr** aims to solve the often overcomplicated problem of tracking the occurance of simple events. Most Todo and note taking software make it unnecessarily complicated to record that an event has happened or will potentially happen. 
<br/><br/> 
 **Trackr** has two primary functions:
 1) **Record that an event has occured:** *Drank a protein shake at 2:00PM*
 2) **Record that an event will (hopefully) happen:** *Drink a protein shake tonight at 8:00PM*

<br>
<br>

## Prerequisites

* node >= 10.x.x


## Features

- Track/Record Events
    - Title
    - Description
    - Time
    - Functions: Duplicate, Generate plan using Tracked as a template

- Plan events
    - Title
    - Description
    - Planned Time
    - Duplicate as a new Tracked event manually or automatically

- Evaluation
    - Measure variance between linked Planned and Tracked events, plot it
    - History <---> Future

- Misc
    - Events can be linked
    - Linked events can be visualized

