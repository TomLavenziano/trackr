import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
    iconfont: 'md',
    theme: {
        primary: '#0B4F6C',
        secondary: '#01BAEF',
        accent: '#20BF55',
        error: '#e83f3a'
    }
});
