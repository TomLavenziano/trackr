import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import Events from './views/Events.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    Base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/events',
            name: 'events',
            component: Events
        },
        {
            path: '/about',
            name: 'about',
            component: About
            // Route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            // component: function() {
            //     return import(/* webpackChunkName: "about" */ './views/About.vue');
            // }
        }
    ]
});
