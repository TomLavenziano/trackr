module.exports = {
    lintOnSave: false,
    publicPath: undefined,
    outputDir: undefined,
    assetsDir: undefined,
    runtimeCompiler: true,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined,

    pwa: {
        name: 'Trackr',
        themeColor: '#0B4F6C',
        msTileColor: '#2d89ef',
        appleMobileWebAppStatusBarStyle: 'black-translucent'
    }
};
