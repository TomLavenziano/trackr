const { json, send } = require('micro');
const { router, get, post, patch, del } = require('microrouter');
const { events } = require('./lib/dbFunc');

require('dotenv').load();


const status = (req, res) => send(res, 200, 'OK');
const defaultRoute = (req, res) => send(res, 200, 'Default response');
const notFound = (req, res) => send(res, 404, 'Not Found');

// CRUD Event routes
const createEvent = async (req, res) => send(res, 200, await events.create(await json(req))); // Restructure req into proper object
const readEvent = async (req, res) => send(res, 200, await events.get(req.params.id));
const updateEvent = async (req, res) => send(res, 200, await events.update(req.params.id, await json(req))); // Restructure req into proper object
const deleteEvent = async (req, res) => send(res, 200, await events.delete(req.params.id));

const routes = [
    get('/', defaultRoute),
    get('/status', status),

    post('/events', createEvent), // Create
    get('/events', readEvent), // Find all
    get('/events/:id', readEvent), // Find one
    patch('/events/:id', updateEvent), // Update
    del('/events/:id', deleteEvent), // Delete

    get('/*', notFound)
];

module.exports = router(...routes);
