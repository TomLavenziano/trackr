require('dotenv').load();
const db = require('./db');

const events = db.get('events');
// Const users = db.get('users')

// Events

const eventOps = {
    get: async (eid = {}) => {
        return events.find(eid);
    },
    create: async ctx => {
        return events.insert(ctx);
    },
    update: async (eid, ctx) => {
        return events.update(eid, ctx);
    },
    delete: async eid => {
        return events.remove(eid);
    }

};
// Users
// const getUsers = async (uid = {}) => {
//     console.log('Getting users...');
//     return db.get('users').find(uid);
// };

module.exports = {
    events: eventOps
    // Users: {
    //     getUsers
    // }
};
