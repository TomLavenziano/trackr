const micro = require('micro');
const test = require('ava');
const listen = require('test-listen');
const request = require('request-promise');

let TEST_ID = '';

// Service
const service = require('../src');

// Spin up a micro instance and return the response
const getConn = async (srv, route = '', method = 'GET', body = {}) => {
    const microInstance = micro(srv);
    const hostname = await listen(microInstance);
    const options = {
        method: method,
        uri: hostname + route,
        body: body,
        resolveWithFullResponse: true,
        json: true
    };
    const response = await request(options);
    return { microInstance, hostname, response };
};


/* -------------------- *
 *        System        *
 * -------------------- */

const db = require('../src/lib/db');

test('404', async t => {
    try {
        const { response } = await getConn(service, '/non-existent');
        t.deepEqual(response.body, 'Not Found');
        t.deepEqual(response.statusCode, 404);
    } catch (e) {
        t.deepEqual(e.statusCode, 404);
    }
});

test.serial('Database connects successfully', async t => {
    // Const state = '';
    await db.then(d => {
        t.deepEqual(d._state, 'open');
    });
    db.close();
});

test.serial('API status', async t => {
    const { response } = await getConn(service, '/status');
    t.deepEqual(response.body, 'OK');
});


/* ----------------------- *
 *        Endpoints        *
 * ----------------------- */


/* ------ Events ------ */

test.serial('Create a new event', async t => {
    const body = {
        title: 'Test Title',
        description: 'Test Description'
    };
    const { response } = await getConn(service, '/events', 'POST', body);
    TEST_ID = response.body._id;
    t.deepEqual(response.body.title, 'Test Title');
    t.deepEqual(response.body.description, 'Test Description');
});

test.serial('Get events', async t => {
    const { response } = await getConn(service, `/events/${TEST_ID}`);
    t.deepEqual(response.body[0]._id, TEST_ID);
    t.deepEqual(response.body[0].title, 'Test Title');
    t.deepEqual(response.body[0].description, 'Test Description');
});

test.serial('Update an existing event', async t => {
    const body = {
        description: 'Modified test description'
    };
    const { response } = await getConn(service, `/events/${TEST_ID}`, 'PATCH', body);
    t.deepEqual(response.body.ok, 1);
});

test.serial('Delete an event', async t => {
    try {
        const { response } = await getConn(service, `/events/${TEST_ID}`, 'DELETE');
        t.deepEqual(response.body.ok, 1);
        t.deepEqual(response.statusCode, 200);
    } catch (e) {
        t.deepEqual(e.statusCode, 404);
    }
});
